# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def rand_time(from, to=Time.now)
  Time.at(rand_in_range(from.to_f, to.to_f))
end

def rand_in_range(from, to)
  rand * (to - from) + from
end
welcome = Welcome.create([{ title: 'The Night Owl', headline: "Here's what's going on", notes: "This week we've got some great specials and bands, come down and kickit with RB!", role: 'main'}])
# event = Event.create([{ band_id: 1, title: 'Brass-grass-roots!', description: "Here's what's going on", start_time: rand_time(1.month.from_now)}])
# event = Event.create([{ band_id: 2, title: 'Mile 8/Old Re-Union!', description: "Seriously NUTZ! You've seen the video, now live it!", start_time: rand_time(1.month.from_now)}])
# event = Event.create([{ band_id: 3, title: 'Thursday Jam with Neely!', description: "Seriously NUTZ!  You never know really, what songs, how much of the songs, who might sit in and a bunch of other questions only answered if you show up.", start_time: rand_time(1.month.from_now)}])
# event = Event.create([{ band_id: 4, title: 'Get Enabled Right yall!,Collin Peterson (mandolin), Carl Roberts III (washboard/percussion), John Holland (banjo), and Anna Holland (vocals..no relation)', description: "Seriously NUTZ!", start_time: rand_time(1.month.from_now)}])
user_admin = User.create!(email: 'nightowlnashville@gmail.com', password: 'lyricbisonthecut', admin: true)
